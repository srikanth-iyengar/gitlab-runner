FROM deadcoder11u2/debian

RUN apt update && apt install git openjdk-11-jdk python3 python3-venv python3-pip gcc g++ maven sudo  -y

RUN curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

RUN chmod +x /usr/local/bin/gitlab-runner

RUN useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

RUN usermod -aG sudo gitlab-runner

RUN gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

COPY start.sh /entrypoint/start.sh

RUN chmod +x /entrypoint/start.sh

RUN mv /home/gitlab-runner/.bash_logout /home/gitlab-runner/.bash_logout.backup

ENTRYPOINT ["/entrypoint/start.sh"]
